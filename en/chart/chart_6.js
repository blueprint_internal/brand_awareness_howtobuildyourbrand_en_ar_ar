
function Chart6(resources)
{
  Chart6.resources = resources;
}
Chart6.prototype = {
  init: function()
  {


    /// background-image:url('course/en/images/chartback/chartback1.jpg');

///SET CHART TYPE FROM JSON  
var barLine = Chartist.Line

var fnstring = Chart6.resources.chart_style

//LOAD AND CREATE ARRAYS
var labelArray = Chart6.resources.nodeText;
var nodeArray = new Array();
var seriesNodes = new Array();
var len = Chart6.resources._nodes.length;

///LOOP THRU DATA
for(var i = 0; i<len; i++) {
nodeArray.push(Chart6.resources._nodes[i].nodeNum);
}

//CREATE SERIES ARRAY OF NODES
for(var n = 0; n<len; n++) {
seriesNodes.push(nodeArray[n]);
}

//Dotted chart class concat
var _dotchartclass =  Chart6.resources._nodotchartclass;
var _dotdotchartclass = String("."+_dotchartclass)
console.log("_dotchartclass = " + _dotdotchartclass);

  ///Booleans
  var stack_Bars = Boolean(Chart6.resources.stack_bars);
  var reverse_Data  = Boolean(Chart6.resources.reverse_data);
  var horizontal_Bars = Boolean(Chart6.resources.horizontal_bars);
  var _showLabel = Boolean(Chart6.resources.showLabel);
  var _donut = Boolean(Chart6.resources.pieDonut);
  var show_area  =  Boolean(Chart6.resources.show_area);
  var show_point = Boolean(Chart6.resources.show_point);


  var  _startAngle = Chart6.resources.pieStartAngle;
  var _total = Chart6.resources.pieTotal;

  //SIZES AND POSITIONS
  var x_pos = Chart6.resources.x_pos;
  var y_pos = Chart6.resources.y_pos;
  var chart_width = Chart6.resources.chart_width;
  var chart_height = Chart6.resources.chart_height;

  var _low =   Chart6.resources.lowestNum;
  var _high =   Chart6.resources.highestNum;


//SMOOTHING
var line_smooth_switch = String(Chart6.resources.line_smooth);
var line_smooth = Chartist.Interpolation.cardinal();

switch (line_smooth_switch) {
case "step": 
line_smooth = Chartist.Interpolation.step();
break;
case "simple": 
line_smooth = Chartist.Interpolation.simple();
break;
case "none": 
line_smooth = Chartist.Interpolation.none();
break;
case "normal": 
line_smooth = Chartist.Interpolation.cardinal();
break;
}

/// CHART TYPE

switch (fnstring) {
case "line": 
barLine = Chartist.Line
break;
case "bar": 
barLine = Chartist.Bar
break;
case "pie": 
barLine = Chartist.Pie
break;
}

///

if(fnstring == "pie"){

var ctline = new barLine(_dotdotchartclass, {

    labels: labelArray,
    series: seriesNodes[0]

  }, {
    
    chartPadding: 10,
    labelOffset: 10,
    labelDirection: 'explode',
    
    width: chart_width,
    height: chart_height,

    low: _low,
    high: _high,

    donut: _donut,
    startAngle: _startAngle,
    total: _total,
    showLabel: _showLabel
    
    },[
    ['screen and (max-width: 500px)', {
    width: 400,
    height: 300
    }]
  ]);

}


if(fnstring == "line" || fnstring == "bar"){


var ctline = new barLine(_dotdotchartclass, {
  labels: labelArray,
  series: seriesNodes
}, {
//
  width: chart_width,
  height: chart_height,

    low: _low,
  high: _high,
  
  stackBars: stack_Bars,
  reverseData: reverse_Data,
  horizontalBars: horizontal_Bars,
  showArea: show_area,
  showPoint: show_point,
  lineSmooth: line_smooth,

  seriesBarDistance: 30,

  scaleMinSpace: 100,
  // Can be set to true or false. If set to true, the scale will be generated with whole numbers only.
  onlyInteger: true,
  // The reference value can be used to make sure that this value will always be on the chart. This is especially useful on bipolar charts where the bipolar center always needs to be part of the chart.
  axisX: {
    // On the x-axis start means top and end means bottom
    position: x_pos
  },
  axisY: {
    // On the y-axis start means left and end means right
    position: y_pos
  }

},[
  ['screen and (max-width: 500px)', {
///
  width: 300,
  height: 300,
  lineSmooth: true,
  horizontalBars: false,
  showArea: false,
  showPoint: true,
  seriesBarDistance: 15

  }]
])
 


}


},

preload: function()
  {


  },

  create: function(evt)
  {
    //Call the animation build function
    this.parent.buildAnimation();
  },

  setupAnimation: function()
    {
      textAr = new Array();
      for(var i = 0; i<3; i++) {

    }

      this.stage.update();
      
  },


  buildAnimation: function()
  {

  }
    ,
     update: function()
  {

  }
}